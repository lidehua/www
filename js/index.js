/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        angular.module('myApp', [
            'ngRoute',
            'myApp.controllers'
        ]).
        config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: 'HomeCtrl'});
            $routeProvider.when('/discover', {templateUrl: 'partials/discover.html', controller: 'DiscoverCtrl'});
            $routeProvider.when('/account', {templateUrl: 'partials/account.html', controller: 'AccountCtrl'});
            $routeProvider.otherwise({redirectTo: '/home'});
        }]);
    }
};

angular.module('myApp', [
    'ngRoute',
    'myApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: 'HomeCtrl'});
    $routeProvider.when('/discover', {templateUrl: 'partials/discover.html', controller: 'DiscoverCtrl'});
    $routeProvider.when('/account', {templateUrl: 'partials/account.html', controller: 'AccountCtrl'});
    $routeProvider.when('/account/userinfo', {templateUrl: 'partials/account-userinfo.html', controller: 'UserinfoCtrl'});
    $routeProvider.when('/account/subject', {templateUrl: 'partials/account-subject.html', controller: 'SubjectAcctCtrl'});
    $routeProvider.when('/account/activity', {templateUrl: 'partials/account-activity.html', controller: 'ActivityAcctCtrl'});
    $routeProvider.when('/account/settings', {templateUrl: 'partials/account-settings.html', controller: 'SettingsCtrl'});
    $routeProvider.when('/forum/:id', {templateUrl: 'partials/forum.html', controller: 'ForumCtrl'});
    $routeProvider.when('/activity', {templateUrl: 'partials/activity-create.html', controller: 'ActivityCtrl'});
    $routeProvider.when('/activity/:id', {templateUrl: 'partials/activity-detail.html', controller: 'ActivityDetailCtrl'});
    $routeProvider.when('/subject', {templateUrl: 'partials/subject-create.html', controller: 'SubjectCtrl'});
    $routeProvider.when('/subject/:id', {templateUrl: 'partials/subject-detail.html', controller: 'SubjectDetailCtrl'});
    $routeProvider.when('/location', {templateUrl: 'partials/location.html', controller: 'LocationCtrl'});
    $routeProvider.when('/portal', {templateUrl: 'partials/portal.html'});
    $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginCtrl'});
    $routeProvider.when('/register', {templateUrl: 'partials/register.html', controller: 'RegisterCtrl'});
    $routeProvider.when('/forgotpwd', {templateUrl: 'partials/forgetpwd.html', controller: 'ForgotpwdCtrl'});
    $routeProvider.otherwise({redirectTo: '/home'});
}]);
