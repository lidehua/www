'use strict';

/* Controllers */

var myApp = angular.module('myApp.controllers', []);

// 父级controller
myApp.controller('IndexCtrl', [ '$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {
    var OPTIONS = {
        '1' : function() {
            $location.path('/activity');
        },
        '2' : function() {
            $location.path('/subject');
        },
        '3' : function() {
            $location.path('/invite');
        },
        '4' : function() {
            $location.path('/location')
        }
    };
    
    $scope.LOCATION = 'location';
    $scope.COORDS = 'coords';
    $scope.USER = 'user';
    
    // 统一处理顶部状态栏和底部导航栏的信息，防止遗漏
    $scope.nav = function(config, options) {
        var defConfig = {
            'title' : '我的小区',
            'home' : '',
            'discover' : '',
            'account' : '',
            'back' : false,
            'backTitle' : '',
            'backUrl' : '',
            'hideNav' : false
        };
        for ( var key in config) {
            defConfig[key] = config[key];
        }
        if (defConfig.back) {
            defConfig.backUrl = defConfig.backUrl || $rootScope.lastPath;
        }
        $rootScope.config = defConfig;
        $rootScope.show = false;
        $rootScope.options = options || [];
        $rootScope.showOption = function(id) {
            $rootScope.show = !$rootScope.show;
            if (id) {
                OPTIONS[id]();
            }
        };
        $rootScope.back = function() {
            $location.path(defConfig.backUrl);
        };
        $rootScope.lastPath = $location.path();
    };
    
    $scope.storage = function(key, value) {
        if (key && value) {
            localStorage.setItem(key, value);
        } else if (key) {
            return localStorage.getItem(key);
        }
    }
    
    $scope.element = function(selector) {
        return angular.element(document.querySelector(selector));
    };
    
    $scope.checkLogin = function() {
        var user = $scope.storage($scope.USER);
        console.log('USER', user);
        if (user) {
            $scope.user = user;
        } else {
            $location.path('/login');
            return true;
        }
    };
    
    var location = $scope.storage($scope.LOCATION);
    console.log('location', location);
    if (!location) {
        $location.path('/location');
    }
    
} ]);
// 主页
myApp.controller('HomeCtrl', [ '$scope', '$location', function($scope, $location) {
    var location = $scope.storage($scope.LOCATION);
    $scope.nav({
        'title' : location,
        'home' : 'active'
    }, [ {
        'text' : '发起活动',
        'id' : '1'
    }, {
        'text' : '发表话题',
        'id' : '2'
    }, {
        'text' : '邀请邻居',
        'id' : '3'
    }, {
        'text' : '更换小区',
        'id' : '4'
    } ]);
    
    $scope.jumpToForum = function(id) {
        $location.path('/forum/' + id)
    };
} ]);
// 发现
myApp.controller('DiscoverCtrl', [ '$scope', function($scope) {
    $scope.nav({
        'title' : '发现',
        'discover' : 'active'
    });
} ]);
// 我
myApp.controller('AccountCtrl', [ '$scope', '$location', function($scope, $location) {
    if ($scope.checkLogin()) {
        return;
    }
    $scope.nav({
        'title' : '我',
        'account' : 'active',
    });
    $scope.userinfo = function() {
        $location.path('/account/userinfo');
    };
    $scope.favor = function(type) {
        $location.path('/account/' + type);
    };
    $scope.settings = function() {
        $location.path('/account/settings');
    };
    console.log('account')
    $scope.user = {
        'uid' : '1',
        'imgurl' : '/img/1.jpeg'
    };
} ]);
// 用户信息
myApp.controller('UserinfoCtrl', [ '$scope', '$http', function($scope, $http) {
    $scope.nav({
        'title' : '我的信息',
        'back' : true,
        'backTitle' : '我',
        'backUrl' : '/account'
    });
    console.log("userinfo")
    $scope.user = {
        'nickname' : '我是昵称',
        'uid' : '1',
        'imgurl' : '/img/1.jpeg'
    };
    $scope.modifyAvatar = function() {
    };
    $scope.modify = function(key) {
        console.log(key)
        $scope['modify' + key] = true;
    }
} ]);
// 话题
myApp.controller('SubjectCtrl', [ '$scope', '$http', function($scope, $http) {
    $scope.nav({
        'title' : '新建话题',
        'back' : true,
    });
    $scope.subjects = [];
} ]);
myApp.controller('SubjectAcctCtrl', [ '$scope', '$http', function($scope, $http) {
    $scope.nav({
        'title' : '我的话题',
        'back' : true,
        'backTitle' : '我',
        'backUrl' : '/account'
    });
    $scope.subjects = [];
} ]);
myApp.controller('SubjectDetailCtrl', [ '$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
    $scope.nav({
        'title' : '话题',
        'back' : true
    });
    $scope.doReply = function() {
        $scope.reply = true;
    };
    $scope.postReply = function() {
        alert('敬请期待');
    }
    var id = $routeParams.id;
    $scope.subject = {
        'user' : {
            'icon' : '/img/forum-icon.png',
            'nick' : '话题用户1'
        },
        'id' : id,
        'title' : '这是测试标题这是测试标题这是测试标题这是测试标题',
        'content' : '现在这个应用功能完备了，但是还不太好看。而且随着我们继续添加新功能，代码也会膨胀，变得难以管理。我们可以创建自己的指令属性，来帮助我们减少复杂性。',
        'imgUrl' : 'http://pic.yupoo.com/dapenti/DQNshgEn/HytEc.jpg',
        'time' : '18:24'
    };
    $scope.replies = [ {
        'user' : {
            'icon' : '/img/forum-icon.png',
            'nick' : '回复用户1'
        },
        'id' : id,
        'content' : '现在这个应用功能完备了，但是还不太好看。而且随着我们继续添加新功能，代码也会膨胀，变得难以管理。我们可以创建自己的指令属性，来帮助我们减少复杂性。',
        'time' : '10分钟前'
    }, {
        'user' : {
            'icon' : '/img/forum-icon.png',
            'nick' : '回复用户2'
        },
        'id' : id,
        'content' : '现在这个应用功能完备了，但是还不太好看。而且随着我们继续添加新功能，代码也会膨胀，变得难以管理。我们可以创建自己的指令属性，来帮助我们减少复杂性。',
        'imgUrl' : 'http://pic.yupoo.com/dapenti/DQNshgEn/HytEc.jpg',
        'time' : '18:33'
    } ];
} ]);
// 活动
myApp.controller('ActivityCtrl', [ '$scope', '$http', function($scope, $http) {
    $scope.nav({
        'title' : '发起活动',
        'back' : true
    });
    console.log($scope)
} ]);
myApp.controller('ActivityAcctCtrl', [ '$scope', '$http', function($scope, $http) {
    $scope.nav({
        'title' : '我的活动',
        'back' : true,
        'backTitle' : '我',
        'backUrl' : '/account'
    });
    console.log($scope)
    $scope.activities = [];
} ]);
myApp.controller('ActivityDetailCtrl', [ '$scope', '$http', function($scope, $http) {
    $scope.nav({
        'title' : '活动',
        'back' : true
    });
    console.log($scope)
    $scope.activity = [];
} ]);
// 设置
myApp.controller('SettingsCtrl', [ '$scope', '$http', function($scope, $http) {
    $scope.nav({
        'title' : '设置',
        'back' : true,
        'backTitle' : '我',
        'backUrl' : '/account'
    });
    console.log("settings")
} ]);
// 版块
myApp.controller('ForumCtrl', [ '$scope', '$http', '$routeParams', '$location',
        function($scope, $http, $routeParams, $location) {
            $scope.nav({
                'title' : '测试版块',
                'back' : true,
                'backTitle' : '小区',
                'backUrl' : '/home'
            }, [ {
                'text' : '发起活动',
                'id' : '1'
            }, {
                'text' : '发表话题',
                'id' : '2'
            } ]);
            $scope.id = $routeParams.id;
            $scope.forum = {
                'id' : $routeParams.id,
                'name' : '测试版块'
            };
            console.log("forum" + $routeParams.id)

            $scope.showActivity = function(id) {
                if (id) {
                    console.log('activity' + id)
                    return;
                }
                $scope.activities = [ {
                    'user' : {
                        'icon' : '/img/forum-icon.png',
                        'nick' : '活动用户1'
                    },
                    'id' : 1,
                    'title' : '这是测试标题这是测试标题这是测试标题这是测试标题',
                    'time' : '刚刚',
                    'status' : '报名中'
                }, {
                    'user' : {
                        'icon' : '/img/forum-icon.png',
                        'nick' : '活动用户2'
                    },
                    'id' : 2,
                    'title' : '这是测试标题这是测试标题这是测试标题这是测试标题',
                    'time' : '10分钟前'
                }, {
                    'user' : {
                        'icon' : '/img/forum-icon.png',
                        'nick' : '活动用户3'
                    },
                    'id' : 3,
                    'title' : '这是测试标题这是测试标题这是测试标题这是测试标题',
                    'time' : '13:33'
                } ];
                $scope.subjects = [];
            };
            $scope.showSubject = function(id) {
                if (id) {
                    console.log('subject' + id)
                    $location.path('/subject/' + id);
                    return;
                }
                $scope.activities = [];
                $scope.subjects = [ {
                    'user' : {
                        'icon' : '/img/forum-icon.png',
                        'nick' : '帖子用户1'
                    },
                    'id' : 1,
                    'title' : '这是测试标题这是测试标题这是测试标题这是测试标题',
                    'time' : '刚刚',
                    'reply' : 1
                }, {
                    'user' : {
                        'icon' : '/img/forum-icon.png',
                        'nick' : '帖子用户2'
                    },
                    'id' : 2,
                    'title' : '这是测试标题这是测试标题这是测试标题这是测试标题',
                    'time' : '5分钟前',
                    'reply' : 2
                }, {
                    'user' : {
                        'icon' : '/img/forum-icon.png',
                        'nick' : '帖子用户3'
                    },
                    'id' : 3,
                    'title' : '这是测试标题这是测试标题这是测试标题这是测试标题',
                    'time' : '13:33',
                    'reply' : 3
                } ];
            };

            $scope.showActivity();

        } ]);

myApp.controller('LocationCtrl', [ '$scope', '$http', '$location', function($scope, $http, $location) {
    $scope.nav({
        'title' : '定位小区',
        'hideNav' : true
    });
    //var baiduSearchUrl = 'http://api.map.baidu.com/place/v2/search?ak=EA3848a3abdb182cfc4e710ed64dc1fb&output=json&q=';
    var baiduSearchUrl = 'http://www.xiaoquonline.com/api/1/location/search?ak=EA3848a3abdb182cfc4e710ed64dc1fb&output=json&callback=JSON_CALLBACK&q=';
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            $scope.warning = false;
            $scope.search('小区', position.coords);
            $scope.storage($scope.COORDS, JSON.stringify(position.coords));
        });
    } else {
        console.log("no location")
        $scope.warning = true;
    }
    
    $scope.search = function(keyword, coords) {
        $scope.locs = [];
        if (!keyword) {
            keyword = $scope.element('#keyword').val();
        }
        var url = baiduSearchUrl + encodeURIComponent(keyword);
        coords = coords || JSON.parse($scope.storage($scope.COORDS));
        if (coords) {
            url += '&location=' + coords.latitude + ',' + coords.longitude + '&r=3000';
        } else {
            url += '&region=' + encodeURIComponent('全国');
        }
        $http.get(url).success(function(data) {
            $scope.locs = data.results;
        });
    };
    
    $scope.selectLoc = function(id, name) {
        $scope.storage($scope.LOCATION, name);
        $location.path('/home');
    };
    
    console.log("location")
} ]);
// 登录
myApp.controller('LoginCtrl', [ '$scope', '$http', '$location', function($scope, $http, $location) {
    $scope.nav({
        'title' : '登录',
        'hideNav' : true,
        'back' : true
    });
    
    $scope.login = function() {
        var username = $scope.element('input[name="username"]').val();
        var password = $scope.element('input[name="password"]').val();
        if (username && password) {
            $location.path('/home');
        } else {
            $scope.warning = '用户名或密码不能为空';
        }
    };
}]);
// 注册
myApp.controller('RegisterCtrl', [ '$scope', '$http', '$location', function($scope, $http, $location) {
    $scope.nav({
        'title' : '注册',
        'hideNav' : true,
        'back' : true,
        'backUrl' : '/login'
    });
    
}]);
//忘记密码
myApp.controller('ForgotpwdCtrl', [ '$scope', '$http', '$location', function($scope, $http, $location) {
    $scope.nav({
        'title' : '找回密码',
        'hideNav' : true,
        'back' : true,
        'backUrl' : '/login'
    });
    
}]);